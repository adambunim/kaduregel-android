package com.adambunim.kaduregel.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import coil.load
import coil.transform.CircleCropTransformation
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.databinding.AvatarAndNameViewBinding
import com.adambunim.kaduregel.firestore.Member

@SuppressLint("ViewConstructor")
class AvatarAndNameView(context: Context, member: Member) : LinearLayout(context) {

    init {
        val binding = AvatarAndNameViewBinding.inflate(LayoutInflater.from(context), this, true)
        binding.username.text = member.name
        if (member.picture != null) {
            binding.userAvatar.load(member.picture) {
                transformations(CircleCropTransformation())
            }
        } else {
            binding.userAvatar.load(R.drawable.ic_avatar_placeholder) {
                transformations(CircleCropTransformation())
            }
        }
    }
}