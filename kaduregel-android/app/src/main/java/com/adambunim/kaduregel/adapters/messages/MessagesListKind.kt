package com.adambunim.kaduregel.adapters.messages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adambunim.kaduregel.databinding.DateTitleItemBinding
import com.adambunim.kaduregel.databinding.MessageItemBinding

enum class MessagesListKind {

    DATE_TITLE {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                DateTitleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MessagesDateTitleItem.ViewHolder(binding)
        }
    },
    MESSAGE {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                MessageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MessagesMessageItem.ViewHolder(binding)
        }
    };


    abstract fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}