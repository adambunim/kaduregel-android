package com.adambunim.kaduregel.teams

import com.adambunim.kaduregel.firestore.Game
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.Message
import com.adambunim.kaduregel.firestore.memberRef
import com.adambunim.kaduregel.isGoing
import com.google.firebase.firestore.FieldValue

fun makeTeams(members: List<Member>, messages: List<Message>, game: Game): String? {
    val going = members.filter { isGoing(it.id, messages) }
        .let { list ->
            if (game.maxPlayers != null) {
                list.take(game.maxPlayers.toInt())
            } else {
                list
            }
        }
    val playersWithoutScore = going.filter { it.stars == null || it.stars == 0 }
    if (playersWithoutScore.isNotEmpty()) {
        return "לא לכולם יש ציונים"
    }
    val notGoing = members.filter { !going.contains(it) }
    for (m in notGoing) {
        memberRef(game.id, m.id).update(mapOf(Member.TEAM to FieldValue.delete()))
    }
    val sorted = going.shuffled().sortedBy { it.stars }.reversed()
    val chooseOrder = intArrayOf(1,2,3,3,2,1)
    for (i in sorted.indices) {
        val m: Member = sorted[i]
        val team = chooseOrder[i % chooseOrder.size]
        memberRef(game.id, m.id).update(mapOf(Member.TEAM to team))
    }
    return null
}