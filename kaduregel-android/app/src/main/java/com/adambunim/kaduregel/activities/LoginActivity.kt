package com.adambunim.kaduregel.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.base.BaseActivity
import com.adambunim.kaduregel.databinding.ActivityLoginBinding
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse

class LoginActivity : BaseActivity<ActivityLoginBinding>(ActivityLoginBinding::inflate) {

    private val RC_SIGN_IN = 9001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.signInButton.setOnClickListener { signIn() }
        binding.loginMessage.setText(if (isInvite) R.string.login_title_invite else R.string.login_title_normal)
    }

    private fun signIn() {
        val providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build()
        )

        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build(),
            RC_SIGN_IN
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                if (isInvite) {
                    setResult(RESULT_OK)
                } else {
                    startActivity(Intent(this, GamesActivity::class.java))
                }
                finish()
            } else {
                print("failed")
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

    private val isInvite: Boolean
        get() = intent.getBooleanExtra(KEY_IS_INVITE, false)

    companion object {
        private const val KEY_IS_INVITE = "is_invite"
        fun newIntent(context: Context, isInvite: Boolean): Intent {
            return Intent(context, LoginActivity::class.java).apply {
                putExtra(KEY_IS_INVITE, isInvite)
            }
        }
    }
}