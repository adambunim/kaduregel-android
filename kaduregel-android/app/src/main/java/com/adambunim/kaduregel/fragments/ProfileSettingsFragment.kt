package com.adambunim.kaduregel.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import coil.load
import coil.transform.CircleCropTransformation
import com.adambunim.kaduregel.KEY_GAME_ID
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.base.BaseBottomSheetDialogFragment
import com.adambunim.kaduregel.databinding.ProfileSettingsFragmentBinding
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.viewmodel.MemberViewModel
import com.adambunim.kaduregel.viewmodel.MemberViewModelFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.firebase.auth.UserProfileChangeRequest


class ProfileSettingsFragment :
    BaseBottomSheetDialogFragment<ProfileSettingsFragmentBinding>(
        R.layout.profile_settings_fragment,
        ProfileSettingsFragmentBinding::bind
    ) {

    private lateinit var member: Member
    private lateinit var gameId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gameId = requireArguments().getString(KEY_GAME_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val memberViewModel: MemberViewModel by viewModels {
            MemberViewModelFactory(gameId, PreferencesManager.currentUser()!!.uid)
        }
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        memberViewModel.getMember().observe(
            viewLifecycleOwner,
            { (member, _) ->
                this.member = member
                binding.nameValue.text = member.name
                if (member.picture != null) {
                    binding.profilePicture.load(member.picture) {
                        transformations(CircleCropTransformation())
                    }
                } else {
                    binding.profilePicture.load(R.drawable.ic_avatar_placeholder) {
                        transformations(CircleCropTransformation())
                    }
                }
            })
        binding.editName.setOnClickListener { editName() }
        binding.editProfilePicture.setOnClickListener { editProfilePicture() }
    }

    private fun editName() {
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_profile_name)
            input(hintRes = R.string.edit_profile_name_hint, prefill = member.name) { _, name ->
                val newName = name.toString().trim()
                val updates = mapOf<String, Any>(
                    Member.NAME to newName,
                )
                memberRef(gameId, member.id).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editProfilePicture() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQ_SELECT_PICTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQ_SELECT_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedImageURI: Uri = data?.data!!
                val folderRef = userStorageRef(member.id)
                uploadPicture(requireContext(), selectedImageURI, folderRef) { photoUri ->
                    val photoUrl = photoUri.toString()
                    val updates = mapOf<String, Any>(
                        Member.PICTURE to photoUrl,
                    )
                    memberRef(gameId, member.id).update(updates)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        fun newInstance(gameId: String): ProfileSettingsFragment {
            val args = Bundle().apply {
                putString(KEY_GAME_ID, gameId)
            }
            val fragment = ProfileSettingsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

private const val REQ_SELECT_PICTURE = 1