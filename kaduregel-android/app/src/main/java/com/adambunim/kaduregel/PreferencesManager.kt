package com.adambunim.kaduregel

import android.content.Context
import android.content.SharedPreferences

object PreferencesManager {
    private const val PREFS_NAME = "user"
    private const val UID = "UID"
    private const val DISPLAY_NAME = "displayName"
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun getUid(): String? {
        return preferences.getString(UID, null)
    }

    fun setUid(uid: String) {
        preferences.edit().putString(UID, uid).apply()
    }

    fun getDisplayName(): String? {
        return preferences.getString(DISPLAY_NAME, null)
    }

    fun setDisplayName(displayName: String) {
        preferences.edit().putString(DISPLAY_NAME, displayName).apply()
    }

    fun currentUser(): SavedUser? {
        val uid = getUid()?: return null
        val displayName = getDisplayName()
        return SavedUser(uid, displayName)
    }

}

data class SavedUser(var uid: String, var displayName: String?)