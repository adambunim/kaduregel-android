package com.adambunim.kaduregel.viewmodel

import androidx.lifecycle.*
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.ktx.combineLatest

class MessagesViewModel(gameId: String) : ViewModel() {

    private val liveData = FirestoreCollectionLiveData(messagesRef(gameId).orderBy(Message.CREATED))
        .combineLatest(
            FirestoreCollectionLiveData(membersRef(gameId)),
            FirestoreDocumentLiveData(gameRef(gameId))
        )
        .map { (messagesSnapshot, _, gameSnapshot) ->
            val game = Game.fromSnapshot(gameSnapshot)
            val messages = messagesSnapshot.map { doc ->
                Message.fromSnapshot(doc)
            }
            GameAndMessages(game, messages)
        }
        .distinctUntilChanged()

    fun getGameAndMessages(): LiveData<GameAndMessages> {
        return liveData
    }
}

class MessagesViewModelFactory(private val gameId: String) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MessagesViewModel(gameId) as T
    }

}