package com.adambunim.kaduregel.firestore

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.map
import com.adambunim.kaduregel.BuildConfig
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SavedUser
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.firebase.messaging.FirebaseMessaging
import java.util.Date

fun updateGoing(gameId: String, uid: String, going: Boolean) {
    memberRef(gameId, uid).update(
        mapOf(
            Member.GOING to going,
            Member.STATUS_UPDATE_TIME to now()
        )
    )
}

fun userData(user: SavedUser): Map<String, Any> {
    return mapOf<String, Any>(
        Member.OS to "ANDROID",
        Member.LAST_SEEN to now(),
        Member.LAST_SEEN_DATE to Date(),
        Member.VERSION_NAME to BuildConfig.VERSION_NAME,
        Member.NAME to (user.displayName ?: "")
    )
}

fun updateUserData(gameId: String) {
    val user = PreferencesManager.currentUser() ?: return
    val updates = userData(user)
    val userRef = memberRef(gameId, user.uid)
    userRef.update(updates)

    FirebaseMessaging.getInstance().token.addOnSuccessListener { token ->
        userRef.update(Member.TOKEN, token)
    }
}

fun createGuest(gameId: String, guestName: String, goingMessage: String) {
    membersRef(gameId).add(
        mapOf(
            Member.IS_GUEST to true,
            Member.NAME to guestName,
            Member.GOING to true,
            Member.STATUS_UPDATE_TIME to now()
        )
    )
        .addOnSuccessListener { ref ->
            val guestId = ref.id
            addAttendMessage(goingMessage, gameId, guestId, true)
        }
}

fun openCreateGuestDialog(context: Context, gameId: String) {
    MaterialDialog(context).show {
        title(R.string.create_guest)
        input(hintRes = R.string.new_guest_hint) { _, text ->
            val guestName = text.toString().trim()
            val goingMessage = "$guestName ${context.getString(R.string.going_yes_chat)}"
            createGuest(gameId, guestName, goingMessage)
        }
        positiveButton(R.string.approve)
        negativeButton(R.string.cancel)
    }
}

fun updateAttendingStatus(
    gameId: String,
    attendIsRelevantFor: String,
    text: String,
    going: Boolean
) {
    addAttendMessage(text, gameId, attendIsRelevantFor, going)
    updateGoing(gameId, attendIsRelevantFor, going)
}

fun getTeamColor(context: Context, teamNumber: Int?): Int? {
    val resId = when (teamNumber) {
        1 -> R.color.team_1_color
        2 -> R.color.team_2_color
        3 -> R.color.team_3_color
        else -> null
    }
    return if (resId != null) {
        ResourcesCompat.getColor(context.resources, resId, context.theme)
    } else {
        null
    }
}

fun isMemberClickable(game: Game, members: List<Member>): Boolean {
    val user = members.first { member -> PreferencesManager.currentUser()?.uid == member.id }
    return user.admin || game.usersCanViewProfiles
}

fun isAdminLiveData(gameId: String) =
    FirestoreDocumentLiveData(memberRef(gameId, PreferencesManager.currentUser()!!.uid))
        .map { snapshot ->
            val user = Member.fromSnapshot(snapshot)
            user.admin
        }
        .distinctUntilChanged()