package com.adambunim.kaduregel

import com.adambunim.kaduregel.firestore.*
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

fun isGoing(id: String, messages: List<Message>): Boolean {
    val relevantMessages = messages
        .filter {
            it.going != null && it.attendIsRelevantFor == id && it.deleted != true
        }
        .sortedBy {
            it.created
        }
    if (relevantMessages.isEmpty()) {
        return false
    }
    val last: Message = relevantMessages.last()
    return last.going == true
}

fun getLastGoingMessages(messages: List<Message>): List<Message> {
    return messages
        .sortedBy { it.created }
        .filter { it.going != null && it.deleted != true }
        .groupBy { it.attendIsRelevantFor }
        .values.map { list -> list.last() }
        .filter { it.going == true }
        .sortedBy { it.created }
}

fun getMessageBadges(lastGoingMessages: List<Message>): Map<String,Int> {
    if (lastGoingMessages.isEmpty()) {
        return emptyMap()
    }
    val dict = mutableMapOf<String, Int>()
    for (i in lastGoingMessages.indices) {
        val messageId: String = lastGoingMessages[i].id
        dict[messageId] = i
    }
    return dict
}
