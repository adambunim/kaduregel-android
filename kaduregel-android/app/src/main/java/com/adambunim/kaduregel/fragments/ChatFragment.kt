package com.adambunim.kaduregel.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.adambunim.kaduregel.KEY_GAME_ID
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.adapters.VerticalSpaceItemDecoration
import com.adambunim.kaduregel.adapters.messages.MessagesAdapter
import com.adambunim.kaduregel.adapters.messages.MessagesDateTitleItem
import com.adambunim.kaduregel.adapters.messages.MessagesMessageItem
import com.adambunim.kaduregel.base.BaseFragment
import com.adambunim.kaduregel.databinding.ChatFragmentBinding
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.getLastGoingMessages
import com.adambunim.kaduregel.getMessageBadges
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.ktx.hideKeyboard
import com.adambunim.kaduregel.viewmodel.MembersViewModel
import com.adambunim.kaduregel.viewmodel.MembersViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory
import org.joda.time.DateTime
import org.joda.time.Days

class ChatFragment :
    BaseFragment<ChatFragmentBinding>(
        R.layout.chat_fragment,
        ChatFragmentBinding::bind
    ) {

    private lateinit var gameId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = requireArguments()
        gameId = bundle.getString(KEY_GAME_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        val user = PreferencesManager.currentUser()
        if (user != null) {
            val viewModel: MembersViewModel by viewModels {
                MembersViewModelFactory(
                    gameId, requireContext().getSharedPreferences(
                        SHARED_PREFS_NAME, Context.MODE_PRIVATE
                    )
                )
            }
            viewModel.getGameAndMembers().observe(
                viewLifecycleOwner
            ) { (_, members, _, isAdmin) ->
                val messagesViewModel: MessagesViewModel by viewModels {
                    MessagesViewModelFactory(
                        gameId
                    )
                }
                messagesViewModel.getGameAndMessages().observe(
                    viewLifecycleOwner
                ) { (game, messages) ->
                    handleMessages(game, messages, members, isAdmin)
                    setGoingButtonsState(isGoing(user.uid, messages))
                }
            }
        }
        binding.send.isEnabled = false
        val drawable = ResourcesCompat.getDrawable(
            resources,
            R.drawable.bg_chat_input,
            requireActivity().theme
        )
        binding.text.background = drawable
        binding.text.doOnTextChanged { text, _, _, _ ->
            binding.send.isEnabled = !text.isNullOrEmpty()
        }
        binding.send.setOnClickListener { writeMessage() }
        binding.goingButton.setOnClickListener {
            updateAttendingStatus(
                gameId,
                PreferencesManager.currentUser()!!.uid,
                getString(R.string.going_yes_chat),
                true
            )
        }
        binding.notGoingButton.setOnClickListener {
            updateAttendingStatus(
                gameId,
                PreferencesManager.currentUser()!!.uid,
                getString(R.string.going_no_chat),
                false
            )
        }
        binding.handleGuestsButton.setOnClickListener {
            val guestsFragment = GuestsFragment.newInstance(gameId)
            guestsFragment.show(parentFragmentManager, "guests_dialog")
        }
        binding.text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                writeMessage()
                true
            } else {
                false
            }
        }
    }

    private fun handleMessages(game: Game, messages: List<Message>, members: List<Member>, isAdmin: Boolean) {
        binding.messageList.isVisible = messages.isNotEmpty()
        binding.messagesEmpty.isVisible = messages.isEmpty()
        val lastGoingMessages = getLastGoingMessages(messages)
        val badges = getMessageBadges(lastGoingMessages)
        val messagesItems = messages.groupBy { message ->
            DateTime(message.created).withTimeAtStartOfDay()
        }
            .map { (date, messages) ->
                val dateTitle = when (Days.daysBetween(DateTime.now(), date).days) {
                    0 -> getString(R.string.today)
                    -1 -> getString(R.string.yesterday)
                    else -> "${date.dayOfMonth().get()}.${date.monthOfYear().get()}"
                }
                listOf(MessagesDateTitleItem(dateTitle)).plus(messages.map { message ->
                    val badge = badges[message.id]
                    val badgeType =
                        if (game.maxPlayers != null && badge != null && badge.toInt() > game.maxPlayers) {
                            MessagesMessageItem.BadgeType.Overbooked
                        } else {
                            MessagesMessageItem.BadgeType.Normal
                        }
                    MessagesMessageItem(gameId, message, members, isAdmin, badge, badgeType)
                })
            }
            .flatten()
        val adapter = binding.messageList.adapter as MessagesAdapter
        adapter.submitList(messagesItems)
        if (messagesItems.isNotEmpty()) {
            binding.messageList.itemAnimator?.isRunning {
                binding.messageList.smoothScrollToPosition(messagesItems.lastIndex)
            }
        }
    }

    private fun setupRecycler() {
        binding.messageList.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            linearLayoutManager.stackFromEnd = true
            layoutManager = linearLayoutManager
            adapter = MessagesAdapter()
            addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.divider_height_chat)))
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }
    }

    private fun writeMessage() {
        val text = binding.text.text.toString().trim()
        addTextMessage(text, gameId)
        notifyUsers(text, gameId)
        binding.text.setText("")
        binding.text.hideKeyboard()
    }

    private fun setGoingButtonsState(going: Boolean?) {
        binding.goingButton.isChecked = going == true
        binding.notGoingButton.isChecked = going == false
    }

}