package com.adambunim.kaduregel.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.adambunim.kaduregel.KEY_GAME_ID
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.base.BaseActivity
import com.adambunim.kaduregel.databinding.ActivityTeamsBinding
import com.adambunim.kaduregel.firestore.getTeamColor
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.viewmodel.MembersViewModel
import com.adambunim.kaduregel.viewmodel.MembersViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory

class TeamsActivity : BaseActivity<ActivityTeamsBinding>(ActivityTeamsBinding::inflate) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val gameId = intent.getStringExtra(KEY_GAME_ID)!!
        val viewModel: MembersViewModel by viewModels {
            MembersViewModelFactory(
                gameId,
                getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            )
        }
        viewModel.getGameAndMembers().observe(
            this
        ) { (game, members, _, isAdmin) ->
            val messagesViewModel: MessagesViewModel by viewModels { MessagesViewModelFactory(gameId) }
            messagesViewModel.getGameAndMessages().observe(this
            ) { (game, messages) ->
                val membersClickable = isAdmin || game.usersCanViewProfiles
                val teams = members
                    .filter {
                        isGoing(it.id, messages) && it.team != null
                    }
                    .groupBy { it.team }
                val team1Color = getTeamColor(this, 1)!!
                val team2Color = getTeamColor(this, 2)!!
                val team3Color = getTeamColor(this, 3)!!
                binding.team1.bind(team1Color, teams[1] ?: emptyList(), membersClickable, game)
                binding.team2.bind(team2Color, teams[2] ?: emptyList(), membersClickable, game)
                binding.team3.bind(team3Color, teams[3] ?: emptyList(), membersClickable, game)
            }
        }
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    companion object {

        fun newIntent(context: Context, gameId: String): Intent {
            return Intent(context, TeamsActivity::class.java).apply {
                putExtra(KEY_GAME_ID, gameId)
            }
        }
    }
}