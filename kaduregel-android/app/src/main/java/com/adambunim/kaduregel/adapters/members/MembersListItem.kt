package com.adambunim.kaduregel.adapters.members

import android.content.res.ColorStateList
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.adapters.messages.MessagesMessageItem
import com.adambunim.kaduregel.databinding.MemberItemBinding
import com.adambunim.kaduregel.databinding.MembersTitleBinding
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.getTeamColor
import com.adambunim.kaduregel.fragments.MemberFragment
import com.adambunim.kaduregel.ktx.activity

abstract class MembersListItem(val kind: MembersListKind) {

    abstract fun bindViewHolder(holder: RecyclerView.ViewHolder)
    abstract val id: String
}

data class MembersTitleItem(private val title: String, private val bgColor: Int) :
    MembersListItem(MembersListKind.TITLE) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(title, bgColor)
    }

    override val id: String
        get() = title

    class ViewHolder(private val binding: MembersTitleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(title: String, bgColor: Int) {
            binding.membersTitle.setBackgroundColor(bgColor)
            binding.membersTitle.text = title
        }
    }
}

data class MembersMemberItem(
    private val gameId: String,
    private val member: Member,
    private val isClickable: Boolean,
    private val showRanks: Boolean
) :
    MembersListItem(MembersListKind.MEMBER) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(gameId, member, isClickable, showRanks)
    }

    override val id: String
        get() = member.id

    class ViewHolder(private val binding: MemberItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var gameId: String
        private lateinit var member: Member

        init {
            binding.root.setOnClickListener {
                val activity = binding.root.activity
                val memberFragment = MemberFragment.newInstance(gameId, member.id)
                memberFragment.show(activity.supportFragmentManager, "member_dialog")
            }
        }

        fun bind(
            gameId: String,
            member: Member,
            isClickable: Boolean,
            showRanks: Boolean
        ) {
            binding.root.isClickable = isClickable
            binding.ranksWrapper.isVisible = showRanks
            this.gameId = gameId
            this.member = member
            binding.username.text = member.name
            setupChip()

            val color: Int? = getTeamColor(binding.root.context, member.team?.toInt())
            if (color != null) {
                binding.teamShirt.imageTintList = ColorStateList.valueOf(color)
                binding.teamWrapper.isVisible = true
            } else {
                binding.teamWrapper.isVisible = false
            }


            if (member.picture != null) {
                binding.imageView.load(member.picture) {
                    transformations(CircleCropTransformation())
                }
            } else {
                binding.imageView.load(R.drawable.ic_avatar_placeholder) {
                    transformations(CircleCropTransformation())
                }
            }
            binding.memberStars.text = member.stars?.toString() ?: "-"
        }

        private fun setupChip() {
            when {
                member.admin -> {
                    binding.memberChip.isVisible = true
                    binding.memberChip.setChipIconResource(R.drawable.ic_shield_key)
                    binding.memberChip.setText(R.string.admin)
                }
                member.isGuest -> {
                    binding.memberChip.isVisible = true
                    binding.memberChip.chipIcon = null
                    binding.memberChip.setText(R.string.guest)
                }
                else -> {
                    binding.memberChip.isVisible = false
                }
            }
        }

    }

}