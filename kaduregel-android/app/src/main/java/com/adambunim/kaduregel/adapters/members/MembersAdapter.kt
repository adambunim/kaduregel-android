package com.adambunim.kaduregel.adapters.members

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class MembersAdapter :
    ListAdapter<MembersListItem, RecyclerView.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MembersListKind.values()[viewType].createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position).bindViewHolder(holder)
    }

    override fun getItemViewType(position: Int): Int = getItem(position).kind.ordinal

}

private val diffCallback = object : DiffUtil.ItemCallback<MembersListItem>() {

    override fun areItemsTheSame(oldItem: MembersListItem, newItem: MembersListItem): Boolean =
        oldItem.id == newItem.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: MembersListItem, newItem: MembersListItem): Boolean =
        oldItem == newItem

}