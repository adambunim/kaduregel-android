package com.adambunim.kaduregel.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import com.adambunim.kaduregel.*
import com.adambunim.kaduregel.adapters.game.GamePagerAdapter
import com.adambunim.kaduregel.base.BaseActivity
import com.adambunim.kaduregel.databinding.ActivityGameBinding
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.fragments.EditGameFragment
import com.adambunim.kaduregel.fragments.ProfileSettingsFragment
import com.adambunim.kaduregel.teams.makeTeams
import com.adambunim.kaduregel.viewmodel.IsAdminViewModel
import com.adambunim.kaduregel.viewmodel.IsAdminViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class GameActivity : BaseActivity<ActivityGameBinding>(ActivityGameBinding::inflate) {

    lateinit var gameId: String
    lateinit var game: Game
    private var messages: List<Message> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        gameId = intent.getStringExtra(KEY_GAME_ID)!!
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        binding.toolbar.inflateMenu(R.menu.view_event_menu)
        binding.toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.edit -> {
                    editClicked()
                    true
                }
                R.id.profile_settings -> {
                    profileSettingsClicked()
                    true
                }
                R.id.clear -> {
                    clearClicked()
                    true
                }
                R.id.inviteViaLink -> {
                    inviteViaLinkClicked()
                    true
                }
                R.id.teams -> {
                    createTeamsClicked()
                    true
                }
                R.id.viewTeams -> {
                    viewTeams()
                    true
                }
                R.id.delete -> {
                    deleteClicked()
                    true
                }
                else -> false
            }
        }
        val bundle = intent.extras
        binding.pager.adapter = GamePagerAdapter(this, bundle)
        TabLayoutMediator(binding.slidingTabs, binding.pager) { tab, position ->
            val title = if (position == 0) {
                getString(R.string.tab_title_chat)
            } else {
                getString(R.string.tab_title_members)
            }
            tab.text = title
        }.attach()
        val messagesViewModel: MessagesViewModel by viewModels { MessagesViewModelFactory(gameId) }
        messagesViewModel.getGameAndMessages().observe(this
        ) { (game, messages) ->
            this.game = game
            this.messages = messages
            binding.toolbar.title = game.name
            binding.toolbar.menu.findItem(R.id.inviteViaLink).isVisible =
                !game.link.isNullOrEmpty()
        }
        val isAdminViewModel: IsAdminViewModel by viewModels { IsAdminViewModelFactory(gameId) }
        isAdminViewModel.getIsAdmin().observe(this
        ) { isAdmin ->
            binding.toolbar.menu.findItem(R.id.edit).isVisible = isAdmin
            binding.toolbar.menu.findItem(R.id.clear).isVisible = isAdmin
            binding.toolbar.menu.findItem(R.id.teams).isVisible = isAdmin
            binding.toolbar.menu.findItem(R.id.delete).isVisible = isAdmin
        }
    }

    override fun onStart() {
        super.onStart()
        updateUserData(gameId)
    }

    private fun editClicked() {
        val editGameFragment = EditGameFragment.newInstance(gameId)
        editGameFragment.show(supportFragmentManager, "guests_dialog")
    }

    private fun clearClicked() {
        MaterialDialog(this).show {
            title(R.string.delete_messages)
            positiveButton(R.string.delete) {
                clearChat()
            }
            negativeButton(R.string.cancel)
        }
    }

    private fun profileSettingsClicked() {
        val profileSettingsFragment = ProfileSettingsFragment.newInstance(gameId)
        profileSettingsFragment.show(supportFragmentManager, "profile_settings_dialog")
    }

    private fun inviteViaLinkClicked() {
        val link = "https://${INVITE_SERVER_HOST}/?${INVITE_PARAM_KEY_GAME_ID}=${game.id}"
        val sendIntent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, link)
        }
        startActivity(sendIntent)
    }

    private fun createTeamsClicked() {
        MaterialDialog(this).show {
            title(R.string.divide_to_groups_dialog_title)
            positiveButton(R.string.divide) {
                createTeams()
            }
            negativeButton(R.string.cancel)
        }
    }

    private fun createTeams() {
        membersRef(gameId).get().addOnSuccessListener { documents ->
            val members = documents.map { Member.fromSnapshot(it) }
            messagesRef(gameId).get().addOnSuccessListener { messageDocuments ->
                val messages = messageDocuments.map { Message.fromSnapshot(it) }
                val errorMessage = makeTeams(members, messages, game)
                if (errorMessage != null)
                    Snackbar.make(binding.appBarLayout, errorMessage, Snackbar.LENGTH_LONG).show()
                else
                    Snackbar.make(
                        binding.appBarLayout,
                        R.string.team_creation_success_message,
                        Snackbar.LENGTH_LONG
                    )
                        .setAction(R.string.team_creation_success_action) {
                            viewTeams()
                        }.show()
            }
        }
    }


    private fun viewTeams() {
        startActivity(TeamsActivity.newIntent(this, gameId))
    }

    private fun deleteClicked() {
        MaterialDialog(this).show {
            title(R.string.delete_group)
            positiveButton(R.string.delete) {
                deleteGame()
            }
            negativeButton(R.string.cancel)
        }
    }

    private fun deleteGame() {
        messagesRef(gameId).get().addOnSuccessListener { documents ->
            Firebase.firestore.runBatch { batch ->
                documents.forEach { messageSnapshot -> batch.delete(messageSnapshot.reference) }
            }
        }
        membersRef(gameId).get().addOnSuccessListener { documents ->
            Firebase.firestore.runBatch { batch ->
                documents.forEach { messageSnapshot -> batch.delete(messageSnapshot.reference) }
            }
        }
        gameRef(gameId).delete()
        finish()
    }

    private fun clearChat() {
        messagesRef(gameId).get().addOnSuccessListener { documents ->
            Firebase.firestore.runBatch { batch ->
                documents.forEach { messageSnapshot -> batch.delete(messageSnapshot.reference) }
            }
            addTextMessage(getString(R.string.delete_messages_message), gameId)
        }
        membersRef(gameId).get().addOnSuccessListener { documents ->
            val updates = hashMapOf<String, Any>(
                Member.GOING to FieldValue.delete(),
                Member.TEAM to FieldValue.delete(),
            )
            for (document in documents) {
                document.reference.update(updates)
            }
        }
    }

}