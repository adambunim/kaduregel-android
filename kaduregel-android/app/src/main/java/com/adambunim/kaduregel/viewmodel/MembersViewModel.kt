package com.adambunim.kaduregel.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.*
import com.adambunim.kaduregel.KEY_SHOW_RANKS_IN_MEMBERS_LIST
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.ktx.combineLatest

class MembersViewModel(gameId: String, sharedPreferences: SharedPreferences) : ViewModel() {

    private val liveData = FirestoreCollectionLiveData(membersRef(gameId))
        .combineLatest(
            FirestoreDocumentLiveData(gameRef(gameId)),
            isAdminLiveData(gameId),
            sharedPreferences.booleanLiveData(KEY_SHOW_RANKS_IN_MEMBERS_LIST, true)
        )
        .map { (membersSnapshot, gameSnapshot, isAdmin, showRanksPref) ->
            val members = membersSnapshot.map { doc ->
                Member.fromSnapshot(doc)
            }
            val game = Game.fromSnapshot(gameSnapshot)
            MembersWrapper(game, members, isAdmin && showRanksPref, isAdmin)
        }
        .distinctUntilChanged()

    fun getGameAndMembers(): LiveData<MembersWrapper> {
        return liveData
    }
}

class MembersViewModelFactory(
    private val gameId: String,
    private val sharedPreferences: SharedPreferences
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MembersViewModel(gameId, sharedPreferences) as T
    }

}