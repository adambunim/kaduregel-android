package com.adambunim.kaduregel.firestore

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.google.firebase.storage.StorageReference
import java.io.ByteArrayOutputStream
import java.util.*

fun uploadPicture(
    context: Context,
    uri: Uri,
    folderRef: StorageReference,
    onSuccess: (photoUri: Uri) -> Unit
) {
    val bitmap = decodeScaledBitmap(context, uri, 500)
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
    val imageData: ByteArray = stream.toByteArray()
    val filename = "${UUID.randomUUID()}.jpg"
    val ref = folderRef.child(filename)
    val uploadTask = ref.putBytes(imageData)
    uploadTask
        .addOnSuccessListener {
            ref.downloadUrl
                .addOnSuccessListener { uri ->
                    onSuccess(uri)
                }
        }
}

private fun decodeScaledBitmap(context: Context, uri: Uri, requiredSize: Int): Bitmap {
    val boundsOptions = BitmapFactory.Options()
    boundsOptions.inJustDecodeBounds = true
    BitmapFactory.decodeStream(
        context.contentResolver.openInputStream(uri),
        null,
        boundsOptions
    )
    var tempWidth = boundsOptions.outWidth
    var tempHeight = boundsOptions.outHeight
    var scale = 1
    while (true) {
        if (tempWidth / 2 < requiredSize || tempHeight / 2 < requiredSize) break
        tempWidth /= 2
        tempHeight /= 2
        scale *= 2
    }
    val options = BitmapFactory.Options()
    options.inSampleSize = scale
    return BitmapFactory.decodeStream(
        context.contentResolver.openInputStream(uri),
        null,
        options
    )!!
}