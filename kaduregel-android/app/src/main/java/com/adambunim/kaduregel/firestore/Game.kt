package com.adambunim.kaduregel.firestore

import android.os.Parcelable
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.parcelize.Parcelize

@Parcelize
data class Game(
    val id: String,
    val name: String?,
    val memberIds: List<String>,
    val link: String?,
    val maxPlayers: Long?,
    val usersCanViewProfiles: Boolean,
    val picture: String?,
    val maximumRating: Int?
) : Parcelable {

    companion object {

        val NAME = "name"
        val MEMBER_IDS = "memberIds"
        val LINK = "link"
        val MAX_PLAYERS = "maxPlayers"
        val USERS_CAN_VIEW_PROFILES = "usersCanViewProfiles"
        val PICTURE = "picture"
        val MAXIMUN_RATING = "maximumRating"

        fun fromSnapshot(doc: DocumentSnapshot): Game {
            return Game(
                id = doc.id,
                name = doc.getString(NAME),
                memberIds = doc.get(MEMBER_IDS) as? List<String> ?: emptyList(),
                link = doc.getString(LINK),
                maxPlayers = doc.getLong(MAX_PLAYERS),
                usersCanViewProfiles = doc.getBoolean(USERS_CAN_VIEW_PROFILES) ?: false,
                picture = doc.getString(PICTURE),
                maximumRating = doc.getLong(MAXIMUN_RATING)?.toInt(),
            )
        }
    }
}

