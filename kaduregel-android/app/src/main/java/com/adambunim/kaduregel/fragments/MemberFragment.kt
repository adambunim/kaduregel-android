package com.adambunim.kaduregel.fragments

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.NumberPicker
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import coil.ImageLoader
import coil.memory.MemoryCache
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.base.BaseBottomSheetDialogFragment
import com.adambunim.kaduregel.databinding.MemberFragmentBinding
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.ktx.px
import com.adambunim.kaduregel.viewmodel.GameViewModel
import com.adambunim.kaduregel.viewmodel.GameViewModelFactory
import com.adambunim.kaduregel.viewmodel.MemberViewModel
import com.adambunim.kaduregel.viewmodel.MemberViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.color.colorChooser
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.list.listItemsSingleChoice

class MemberFragment :
    BaseBottomSheetDialogFragment<MemberFragmentBinding>(
        R.layout.member_fragment,
        MemberFragmentBinding::bind
    ) {
    private lateinit var gameId: String
    private lateinit var memberId: String
    private lateinit var game: Game
    private lateinit var member: Member
    private lateinit var messages: List<Message>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gameId = requireArguments().getString(KEY_GAME_ID)!!
        memberId = requireArguments().getString(KEY_MEMBER_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val memberViewModel: MemberViewModel by viewModels {
            MemberViewModelFactory(gameId, memberId)
        }
        val gameViewModel: GameViewModel by viewModels {
            GameViewModelFactory(gameId, sharedPreferences)
        }
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        gameViewModel.getGameAndShowRanks().observe(viewLifecycleOwner) { (game) ->
            this.game = game
        }
        memberViewModel.getMember().observe(
            viewLifecycleOwner
        ) { (member, isUserAdmin) ->
            this.member = member
            val messagesViewModel: MessagesViewModel by viewModels { MessagesViewModelFactory(gameId) }
            messagesViewModel.getGameAndMessages().observe(
                this
            ) { (_, messages) ->
                this.messages = messages
                val goingText = when (isGoing(member.id, messages)) {
                    true -> R.string.going_yes_chat
                    false -> R.string.going_no_chat
                }
                binding.goingValue.setText(goingText)
            }
            binding.toolbar.title = member.name
            val context = binding.root.context
            val imageLoader = ImageLoader.Builder(context)
                .memoryCache { MemoryCache.Builder(context).maxSizePercent(0.25).build() }
                .crossfade(true)
                .build()
            val request = ImageRequest.Builder(context)
                .transformations(CircleCropTransformation())
                .size(40.px)
                .target(
                    onSuccess = { result ->
                        binding.toolbar.titleMarginStart =
                            context.resources.getDimensionPixelSize(R.dimen.toolbar_logo_margin)
                        binding.toolbar.logo = result
                    }
                )
            request.data(member.picture ?: R.drawable.ic_avatar_placeholder)
            imageLoader.enqueue(request.build())
            binding.starsRating.text = member.stars?.toString() ?: "-"
            val color: Int? = getTeamColor(context, member.team?.toInt())
            if (color != null) {
                binding.teamShirt.imageTintList = ColorStateList.valueOf(color)
                binding.teamShirtWrapper.isVisible = true
                binding.noTeamLabel.isVisible = false
            } else {
                binding.teamShirtWrapper.isVisible = false
                binding.noTeamLabel.isVisible = true
            }
            val permissionsTextRes = when {
                member.isGuest -> R.string.guest
                member.admin -> R.string.admin
                else -> R.string.normal_user
            }
            binding.adminValue.setText(permissionsTextRes)
            toggleVisibilityOfEditButtons(member, isUserAdmin)
        }
        binding.editStars.setOnClickListener { editStars() }
        binding.editTeam.setOnClickListener { editTeam() }
        binding.editGoing.setOnClickListener { editGoing() }
        binding.editAdmin.setOnClickListener { editAdmin() }
    }

    private fun toggleVisibilityOfEditButtons(member: Member, userAdmin: Boolean) {
        binding.editStars.isInvisible = !userAdmin
        binding.editTeam.isInvisible = !userAdmin
        binding.editGoing.isInvisible = !userAdmin
        binding.editAdmin.isInvisible = !userAdmin || member.isGuest
    }

    private fun editStars() {
        val dialog = MaterialDialog(requireContext())
            .title(R.string.title_edit_stars)
            .positiveButton(R.string.save) { dialog ->
                val picker: NumberPicker = dialog.getCustomView().findViewById(R.id.stars)
                val updates = mapOf<String, Any>(
                    Member.STARS to picker.value,
                )
                memberRef(gameId, memberId).update(updates)
            }
            .negativeButton(R.string.cancel)
            .customView(R.layout.dialog_stars)
        val picker: NumberPicker = dialog.getCustomView().findViewById(R.id.stars)
        picker.minValue = 1
        picker.maxValue = game.maximumRating ?: 3
        picker.value = member.stars ?: 1
        dialog.show()
    }

    private fun editTeam() {
        val color1 =
            ResourcesCompat.getColor(resources, R.color.team_1_color, requireActivity().theme)
        val color2 =
            ResourcesCompat.getColor(resources, R.color.team_2_color, requireActivity().theme)
        val color3 =
            ResourcesCompat.getColor(resources, R.color.team_3_color, requireActivity().theme)
        val colors = intArrayOf(color1, color2, color3)
        val initialColor = getTeamColor(requireContext(), member.team?.toInt())
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_team)
            colorChooser(colors, initialSelection = initialColor) { _, color ->
                val team = colors.indexOf(color) + 1
                val updates = mapOf<String, Any>(
                    Member.TEAM to team.toLong(),
                )
                memberRef(gameId, memberId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editGoing() {
        val initialSelection = when (isGoing(member.id, messages)) {
            true -> 0
            false -> 1
        }
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_going)
            listItemsSingleChoice(
                R.array.going_statuses,
                initialSelection = initialSelection
            ) { _, index, _ ->
                val going = index == 0
                val goingSuffix = if (going) R.string.going_yes_chat else R.string.going_no_chat
                val goingMessage =
                    "${member.name} ${binding.root.context.getString(goingSuffix)}"
                updateAttendingStatus(gameId, memberId, goingMessage, going)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editAdmin() {
        val initialSelection = if (member.admin) 0 else 1
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_permissions)
            listItemsSingleChoice(
                R.array.user_permissions_values,
                initialSelection = initialSelection
            ) { _, index, _ ->
                val isAdmin = index == 0
                val updates = mapOf<String, Any>(
                    Member.ADMIN to isAdmin,
                )
                memberRef(gameId, memberId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }


    companion object {
        private const val KEY_GAME_ID = "game_id"
        private const val KEY_MEMBER_ID = "member_id"
        fun newInstance(gameId: String, memberId: String): MemberFragment {
            val args = Bundle().apply {
                putString(KEY_GAME_ID, gameId)
                putString(KEY_MEMBER_ID, memberId)
            }
            val fragment = MemberFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val sharedPreferences
        get() = requireContext().getSharedPreferences(
            SHARED_PREFS_NAME, Context.MODE_PRIVATE
        )

}