package com.adambunim.kaduregel.firestore

import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.SavedUser
import java.text.SimpleDateFormat
import java.util.*

private fun createMessage(text: String, user: SavedUser?): Map<String, Any?>? {
    val displayName = user?.displayName ?: return null
    return mutableMapOf(
        Message.WRITER to user.uid,
        Message.WRITER_PRETTY to displayName,
        Message.CREATED to now(),
        Message.CREATED_PRETTY to nowPretty(),
        Message.TEXT to text
    )
}

fun addTextMessage(text: String, gameId: String) {
    val user = PreferencesManager.currentUser() ?: return
    val message = createMessage(text, user) ?: return
    messagesRef(gameId).add(message)
}

fun addAttendMessage(text: String, gameId: String, attendIsRelevantFor: String, going: Boolean) {
    val user = PreferencesManager.currentUser() ?: return
    val message = (createMessage(text, user) ?: return).toMutableMap()
    message[Message.ATTEND_IS_RELEVANT_FOR] = attendIsRelevantFor
    message[Message.GOING] = going
    messagesRef(gameId).add(message)
}

fun now(): Long {
    return System.currentTimeMillis() / 1000
}

fun nowPretty(): String {
    val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
    return formatter.format(Date())
}
