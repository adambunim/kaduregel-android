package com.adambunim.kaduregel.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.*
import com.adambunim.kaduregel.KEY_SHOW_RANKS_IN_MEMBERS_LIST
import com.adambunim.kaduregel.firestore.FirestoreDocumentLiveData
import com.adambunim.kaduregel.firestore.Game
import com.adambunim.kaduregel.firestore.GameAndShowRanks
import com.adambunim.kaduregel.firestore.gameRef
import com.adambunim.kaduregel.ktx.combineLatest

class GameViewModel(gameId: String, sharedPreferences: SharedPreferences) : ViewModel() {

    private val liveData = FirestoreDocumentLiveData(gameRef(gameId))
        .combineLatest(sharedPreferences.booleanLiveData(KEY_SHOW_RANKS_IN_MEMBERS_LIST, true))
        .map { (snapshot, showRanks) ->
            val game = Game.fromSnapshot(snapshot)
            GameAndShowRanks(game, showRanks)
        }
        .distinctUntilChanged()

    fun getGameAndShowRanks(): LiveData<GameAndShowRanks> {
        return liveData
    }

}

class GameViewModelFactory(
    private val gameId: String,
    private val sharedPreferences: SharedPreferences
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GameViewModel(gameId, sharedPreferences) as T
    }

}