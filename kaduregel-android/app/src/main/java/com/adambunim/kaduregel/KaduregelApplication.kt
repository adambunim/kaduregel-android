package com.adambunim.kaduregel

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import com.adambunim.kaduregel.service.CHANNEL_ID
import com.yariksoffice.lingver.Lingver
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import java.util.*
import kotlin.random.Random

class KaduregelApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        PreferencesManager.init(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            val mode = if (Random.nextBoolean()) {
                AppCompatDelegate.MODE_NIGHT_YES
            } else {
                AppCompatDelegate.MODE_NIGHT_NO
            }
            AppCompatDelegate.setDefaultNightMode(mode)
        }
        Lingver.init(this, Locale("iw", "IL"))
        JodaTimeAndroid.init(this)
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, "כדורגל", importance).apply {
                description = "כדורגל"
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}