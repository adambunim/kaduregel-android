package com.adambunim.kaduregel.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) {
            val title = remoteMessage.data["title"] ?: return
            val body = remoteMessage.data["body"] ?: return
            showNotification(this, title, body, remoteMessage.sentTime.toInt())
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Timber.d("onNewToken()")
    }

}