package com.adambunim.kaduregel.adapters.games

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import com.adambunim.kaduregel.KEY_GAME_ID
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.activities.GameActivity
import com.adambunim.kaduregel.databinding.GameItemBinding
import com.adambunim.kaduregel.databinding.InnerMemberItemBinding
import com.adambunim.kaduregel.firestore.GameAndMembers
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.ktx.px

class GamesAdapter : ListAdapter<GameAndMembers, GamesAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            GameItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val gameAndMembers = getItem(position)
        holder.bind(gameAndMembers)
    }

    inner class ViewHolder(private val binding: GameItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener { view ->
                val context = view.context
                val gameAndMembers = getItem(adapterPosition)
                val intent = Intent(context, GameActivity::class.java)
                intent.putExtra(KEY_GAME_ID, gameAndMembers.game.id)
                context.startActivity(intent)
            }
            binding.membersRecycler.apply {
                layoutManager =
                    LinearLayoutManager(binding.root.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = InnerMembersAdapter()
                itemAnimator = null
            }
        }

        fun bind(gameAndMembers: GameAndMembers) {
            val game = gameAndMembers.game
            val members = gameAndMembers.members.filter { !it.isGuest }
            (binding.membersRecycler.adapter as InnerMembersAdapter).submitList(members)
            binding.gameName.text = game.name
            if (game.picture != null) {
                binding.gameIcon.load(game.picture) {
                    transformations(RoundedCornersTransformation(8.px.toFloat()))
                }
            } else {
                binding.gameIcon.load(R.drawable.ic_football_field)
            }
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<GameAndMembers>() {

            override fun areItemsTheSame(
                oldItem: GameAndMembers,
                newItem: GameAndMembers
            ): Boolean = oldItem.game.id == newItem.game.id

            override fun areContentsTheSame(
                oldItem: GameAndMembers,
                newItem: GameAndMembers
            ): Boolean = oldItem == newItem

        }
    }

}

private class InnerMembersAdapter :
    ListAdapter<Member, InnerMembersAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            InnerMemberItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class ViewHolder(private val binding: InnerMemberItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(member: Member) {
            binding.username.text = member.name
            if (member.picture != null) {
                binding.userAvatar.load(member.picture) {
                    transformations(CircleCropTransformation())
                }
            } else {
                binding.userAvatar.load(R.drawable.ic_avatar_placeholder) {
                    transformations(CircleCropTransformation())
                }
            }
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Member>() {

            override fun areItemsTheSame(oldItem: Member, newItem: Member): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Member, newItem: Member): Boolean =
                oldItem == newItem

        }
    }

}