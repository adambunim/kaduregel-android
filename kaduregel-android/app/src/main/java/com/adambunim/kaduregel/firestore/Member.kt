package com.adambunim.kaduregel.firestore

import com.google.firebase.firestore.DocumentSnapshot
import java.util.Date

data class Member(
    val id: String,
    val name: String?,
    val picture: String?,
    val stars: Int?,
    val team: Long?,
    val admin: Boolean,
    val os: String?,
    val token: String?,
    val lastSeen: Long?,
    val lastSeenDate: Date?,
    val isGuest: Boolean,
    val statusUpdateTime: Long?
) {

    companion object {

        val NAME = "name"
        val PICTURE = "picture"
        val GOING = "going"
        val OFFENSE = "offense"
        val DEFENSE = "defense"
        val STARS = "stars"
        val TEAM = "team"
        val ADMIN = "admin"
        val OS = "os"
        val TOKEN = "token"
        val LAST_SEEN = "lastSeen"
        val LAST_SEEN_DATE = "lastSeenDate"
        val IS_GUEST = "isGuest"
        val VERSION_NAME = "version_name"
        val STATUS_UPDATE_TIME = "statusUpdateTime"

        fun fromSnapshot(doc: DocumentSnapshot): Member {
            return Member(
                id = doc.id,
                name = doc.getString(NAME),
                picture = doc.getString(PICTURE),
                stars = doc.getLong(STARS)?.toInt(),
                team = doc.getLong(TEAM),
                admin = doc.getBoolean(ADMIN) ?: false,
                os = doc.getString(OS),
                token = doc.getString(TOKEN),
                lastSeen = doc.getLong(LAST_SEEN),
                lastSeenDate = doc.getDate(LAST_SEEN_DATE),
                isGuest = doc.getBoolean(IS_GUEST) ?: false,
                statusUpdateTime = doc.getLong(STATUS_UPDATE_TIME),
            )
        }


    }

}