package com.adambunim.kaduregel.adapters.messages

import android.app.AlertDialog
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.adapters.messages.MessagesMessageItem.BadgeType.Normal
import com.adambunim.kaduregel.adapters.messages.MessagesMessageItem.BadgeType.Overbooked
import com.adambunim.kaduregel.databinding.DateTitleItemBinding
import com.adambunim.kaduregel.databinding.MessageItemBinding
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.Message
import com.adambunim.kaduregel.firestore.messageRef
import com.adambunim.kaduregel.ktx.activity

abstract class MessagesListItem(val kind: MessagesListKind) {
    abstract fun bindViewHolder(holder: RecyclerView.ViewHolder)
    abstract val id: String
}

data class MessagesDateTitleItem(private val title: String) :
    MessagesListItem(MessagesListKind.DATE_TITLE) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(title)
    }

    override val id: String
        get() = title

    class ViewHolder(private val binding: DateTitleItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(title: String) {
            binding.dateTitle.text = title
        }
    }
}

data class MessagesMessageItem(private val gameId: String, private val message: Message, private val members: List<Member>, private val isAdmin: Boolean, private val badge: Int?, private val badgeType: BadgeType) :
    MessagesListItem(MessagesListKind.MESSAGE) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(gameId, message, members, isAdmin, badge, badgeType)
    }

    override val id: String
        get() = message.id

    class ViewHolder(private val binding: MessageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(gameId: String, message: Message, members: List<Member>, isAdmin: Boolean, badge: Int?, badgeType: BadgeType) {
            val writer = members.first {
                it.id == message.writer
            }
            binding.username.text = writer.name
            if (message.deleted == true) {
                binding.messageBody.text = itemView.context.getString(R.string.message_deleted)
            }
            else if (message.going != null) {
                binding.messageBody.text = getGoingMessage(message, members, message.going)
            }
            else {
                binding.messageBody.text = message.text
            }
            binding.messageTime.text = message.timeSent
            val picture = message.picture
            if (picture != null) {
                binding.userAvatar.load(picture) {
                    transformations(CircleCropTransformation())
                }
            } else {
                binding.userAvatar.load(R.drawable.ic_avatar_placeholder) {
                    transformations(CircleCropTransformation())
                }
            }
            renderBackground(message)
            renderBadge(badge, badgeType)
            if (shouldAddDeleteListener(message, isAdmin)) {
                addLongClick(gameId, message)
            }
        }

        private fun shouldAddDeleteListener(message: Message, isAdmin: Boolean): Boolean {
            return message.writer == PreferencesManager.currentUser()?.uid ||
                    message.attendIsRelevantFor == PreferencesManager.currentUser()?.uid ||
                    isAdmin
        }

        private fun addLongClick(gameId: String, message: Message) {
            binding.root.setOnLongClickListener {
                AlertDialog.Builder(binding.root.activity)
                    .setTitle(R.string.delete)
                    .setPositiveButton(android.R.string.yes, ) { _, _ -> yesClicked(gameId, message) }
                    .setNegativeButton(android.R.string.no, null)
                    .show()
                true
            }
        }

        private fun yesClicked(gameId: String, message: Message) {
            val updates = hashMapOf<String, Any>(
                Message.DELETED to true
            )
            messageRef(gameId, message.id).update(updates)
        }

        private fun getGoingMessage(message: Message, members: List<Member>, going: Boolean): String {
            if (message.attendIsRelevantFor == message.writer) {
                return if (going) "Going" else "Not going"
            }
            val relevantFor = members.first {
                it.id == message.attendIsRelevantFor
            }
            val name = relevantFor.name
            return if (going) String.format("%s is going", name) else String.format("%s is not going", name)
        }

        private fun renderBackground(message: Message) {
            if (message.deleted == true) {
                binding.messageContainer.setBackgroundResource(R.drawable.bg_chat_not_logged_in)
            }
            else {
                val backgroundColor = when (PreferencesManager.currentUser()?.uid) {
                    null -> R.drawable.bg_chat_not_logged_in
                    message.writer -> R.drawable.bg_chat_user
                    else -> R.drawable.bg_chat_other_participant
                }
                binding.messageContainer.setBackgroundResource(backgroundColor)
            }
        }

        private fun renderBadge(badge: Int?, badgeType: BadgeType) {
            val badgeBg = when (badgeType) {
                Normal -> R.drawable.bg_badge_normal
                Overbooked -> R.drawable.bg_badge_overbooked
            }
            binding.badge.setBackgroundResource(badgeBg)
            if (badge != null) {
                binding.badge.text = (badge + 1).toString()
                binding.badge.isVisible = true
            } else {
                binding.badge.isVisible = false
            }
        }
    }

    enum class BadgeType {
        Normal, Overbooked
    }

}