package com.adambunim.kaduregel.adapters.guests

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class GuestsAdapter :
    ListAdapter<GuestsListItem, RecyclerView.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GuestsListKind.values()[viewType].createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position).bindViewHolder(holder)
    }

    override fun getItemViewType(position: Int): Int = getItem(position).kind.ordinal

}

private val diffCallback = object : DiffUtil.ItemCallback<GuestsListItem>() {

    override fun areItemsTheSame(oldItem: GuestsListItem, newItem: GuestsListItem): Boolean =
        oldItem.id == newItem.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: GuestsListItem, newItem: GuestsListItem): Boolean =
        oldItem == newItem

}