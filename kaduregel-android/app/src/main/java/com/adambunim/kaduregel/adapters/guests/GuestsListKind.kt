package com.adambunim.kaduregel.adapters.guests

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adambunim.kaduregel.databinding.AddGuestItemBinding
import com.adambunim.kaduregel.databinding.GuestItemBinding

enum class GuestsListKind {

    CREATE_GUEST {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                AddGuestItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return GuestsAddGuestItem.ViewHolder(binding)
        }
    },
    GUEST {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                GuestItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return GuestsGuestItem.ViewHolder(binding)
        }
    };


    abstract fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}