package com.adambunim.kaduregel.adapters.guests

import androidx.recyclerview.widget.RecyclerView
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.databinding.AddGuestItemBinding
import com.adambunim.kaduregel.databinding.GuestItemBinding
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.Message
import com.adambunim.kaduregel.firestore.openCreateGuestDialog
import com.adambunim.kaduregel.firestore.updateAttendingStatus
import com.adambunim.kaduregel.fragments.MemberFragment
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.ktx.activity

abstract class GuestsListItem(val kind: GuestsListKind) {

    abstract fun bindViewHolder(holder: RecyclerView.ViewHolder)
    abstract val id: String
}

data class GuestsAddGuestItem(private val gameId: String) :
    GuestsListItem(GuestsListKind.CREATE_GUEST) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(gameId)
    }

    override val id: String
        get() = "GuestsAddGuestItem"

    class ViewHolder(binding: AddGuestItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var gameId: String

        init {
            binding.addGuestButton.setOnClickListener {
                openCreateGuestDialog(binding.root.context, gameId)
            }
        }

        fun bind(gameId: String) {
            this.gameId = gameId
        }
    }
}

data class GuestsGuestItem(
    private val gameId: String,
    private val guest: Member,
    private val messages: List<Message>,
    private val isClickable: Boolean
) :
    GuestsListItem(GuestsListKind.GUEST) {
    override fun bindViewHolder(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).bind(gameId, guest, messages, isClickable)
    }

    override val id: String
        get() = guest.id

    class ViewHolder(private val binding: GuestItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var gameId: String
        private lateinit var member: Member

        init {
            binding.root.setOnClickListener {
                val activity = binding.root.activity
                val memberFragment = MemberFragment.newInstance(gameId, member.id)
                memberFragment.show(activity.supportFragmentManager, "member_dialog")
            }
            binding.goingButton.setOnClickListener {
                val goingMessage =
                    "${member.name} ${binding.root.context.getString(R.string.going_yes_chat)}"
                updateAttendingStatus(
                    gameId,
                    member.id,
                    goingMessage,
                    true
                )

            }
            binding.notGoingButton.setOnClickListener {
                val goingMessage =
                    "${member.name} ${binding.root.context.getString(R.string.going_no_chat)}"
                updateAttendingStatus(
                    gameId,
                    member.id,
                    goingMessage,
                    false
                )
            }
        }

        fun bind(gameId: String, member: Member, messages: List<Message>, isClickable: Boolean) {
            binding.root.isClickable = isClickable
            this.gameId = gameId
            this.member = member
            binding.guestName.text = member.name
            val going = isGoing(member.id, messages)
            binding.goingButton.isChecked = going == true
            binding.notGoingButton.isChecked = going == false

        }
    }

}