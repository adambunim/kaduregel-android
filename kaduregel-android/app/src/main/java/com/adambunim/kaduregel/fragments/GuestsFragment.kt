package com.adambunim.kaduregel.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.adapters.guests.GuestsAdapter
import com.adambunim.kaduregel.adapters.guests.GuestsAddGuestItem
import com.adambunim.kaduregel.adapters.guests.GuestsGuestItem
import com.adambunim.kaduregel.base.BaseBottomSheetDialogFragment
import com.adambunim.kaduregel.databinding.GuestsFragmentBinding
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.Message
import com.adambunim.kaduregel.firestore.isMemberClickable
import com.adambunim.kaduregel.firestore.openCreateGuestDialog
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.viewmodel.MembersViewModel
import com.adambunim.kaduregel.viewmodel.MembersViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory

class GuestsFragment :
    BaseBottomSheetDialogFragment<GuestsFragmentBinding>(
        R.layout.guests_fragment,
        GuestsFragmentBinding::bind
    ) {

    private lateinit var gameId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gameId = requireArguments().getString(KEY_GAME_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.createNewGuest.setOnClickListener {
            openCreateGuestDialog(
                requireContext(),
                gameId
            )
        }
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        val viewModel: MembersViewModel by viewModels {
            MembersViewModelFactory(
                gameId, requireContext().getSharedPreferences(
                    SHARED_PREFS_NAME, Context.MODE_PRIVATE
                )
            )
        }
        setupRecycler()
        viewModel.getGameAndMembers().observe(
            viewLifecycleOwner
        ) { (_, members) ->
            val messagesViewModel: MessagesViewModel by viewModels { MessagesViewModelFactory(gameId) }
            messagesViewModel.getGameAndMessages().observe(
                this
            ) { (game, messages) ->
                val guests = members
                    .filter { member -> member.isGuest }
                    .sortedWith(
                        compareBy(
                            { member ->
                                when (isGoing(member.id, messages)) {
                                    true -> 0
                                    false -> 1
                                }
                            }, { member ->
                                val lastSeen = member.lastSeen
                                if (lastSeen != null) {
                                    -lastSeen
                                } else {
                                    0
                                }
                            })
                    )
                binding.guestsEmpty.isVisible = guests.isEmpty()
                binding.guestsList.isVisible = guests.isNotEmpty()
                val isClickable = isMemberClickable(game, members)
                fillAdapter(guests, messages, isClickable)
            }
        }
    }

    private fun fillAdapter(guests: List<Member>, messages: List<Message>, isClickable: Boolean) {
        val items = listOf(GuestsAddGuestItem(gameId))
            .plus(guests.map { guest -> GuestsGuestItem(gameId, guest, messages, isClickable) })
        val adapter = binding.guestsList.adapter as GuestsAdapter
        adapter.submitList(items)
    }

    private fun setupRecycler() {
        binding.guestsList.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = linearLayoutManager
            adapter = GuestsAdapter()
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

    }

    companion object {
        private const val KEY_GAME_ID = "game_id"
        fun newInstance(gameId: String): GuestsFragment {
            val args = Bundle().apply { putString(KEY_GAME_ID, gameId) }
            val fragment = GuestsFragment()
            fragment.arguments = args
            return fragment
        }
    }

}