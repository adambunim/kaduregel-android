package com.adambunim.kaduregel.firestore

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage

fun gamesRef(): CollectionReference {
    return Firebase.firestore.collection("games")
}

fun gameRef(gameId: String): DocumentReference {
    return gamesRef().document(gameId)
}

fun messagesRef(gameId: String): CollectionReference {
    return gameRef(gameId).collection("messages")
}

fun membersRef(gameId: String): CollectionReference {
    return gameRef(gameId).collection("members")
}

fun memberRef(gameId: String, uid: String): DocumentReference {
    return membersRef(gameId).document(uid)
}

fun messageRef(gameId: String, messageId: String): DocumentReference {
    return messagesRef(gameId).document(messageId)
}

fun userStorageRef(uid: String): StorageReference {
    return Firebase.storage.reference.child("Users").child(uid)
}

fun gameStorageRef(gameId: String): StorageReference {
    return Firebase.storage.reference.child("Games").child(gameId)
}