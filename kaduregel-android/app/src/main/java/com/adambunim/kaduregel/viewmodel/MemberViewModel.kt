package com.adambunim.kaduregel.viewmodel

import androidx.lifecycle.*
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.ktx.combineLatest

class MemberViewModel(gameId: String, uid: String) : ViewModel() {

    private val liveData = FirestoreDocumentLiveData(memberRef(gameId, uid))
        .combineLatest(FirestoreDocumentLiveData(memberRef(gameId, PreferencesManager.currentUser()!!.uid)))
        .map { (memberSnapshot, currentUserSnapshot) ->
            val member = Member.fromSnapshot(memberSnapshot)
            val isUserAdmin = Member.fromSnapshot(currentUserSnapshot).admin
            MemberAndIsUserAdmin(member, isUserAdmin)
        }
        .distinctUntilChanged()

    fun getMember(): LiveData<MemberAndIsUserAdmin> {
        return liveData
    }

}

class MemberViewModelFactory(private val gameId: String, private val uid: String) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MemberViewModel(gameId, uid) as T
    }

}