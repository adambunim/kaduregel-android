package com.adambunim.kaduregel.adapters.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.adambunim.kaduregel.fragments.ChatFragment
import com.adambunim.kaduregel.fragments.MembersFragment

class GamePagerAdapter internal constructor(
    activity: FragmentActivity,
    private val bundle: Bundle?
) :
    FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return if (position == 0) {
            val fragment: Fragment = ChatFragment()
            fragment.arguments = bundle
            fragment
        } else {
            val fragment: Fragment = MembersFragment()
            fragment.arguments = bundle
            fragment
        }
    }
}