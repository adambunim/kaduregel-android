package com.adambunim.kaduregel.ktx

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <A, B> LiveData<A>.combineLatest(b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        addSource(this@combineLatest) {
            if (it == null && value != null) value = null
            lastA = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }

        addSource(b) {
            if (it == null && value != null) value = null
            lastB = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }
    }
}

fun <A, B, C> LiveData<A>.combineLatest(b: LiveData<B>, c: LiveData<C>): LiveData<Triple<A, B, C>> {
    return MediatorLiveData<Triple<A, B, C>>().apply {
        var lastA: A? = null
        var lastB: B? = null
        var lastC: C? = null

        addSource(this@combineLatest) {
            if (it == null && value != null) value = null
            lastA = it
            if (lastA != null && lastB != null && lastC != null) value =
                Triple(lastA!!, lastB!!, lastC!!)
        }

        addSource(b) {
            if (it == null && value != null) value = null
            lastB = it
            if (lastA != null && lastB != null && lastC != null) value =
                Triple(lastA!!, lastB!!, lastC!!)
        }

        addSource(c) {
            if (it == null && value != null) value = null
            lastC = it
            if (lastA != null && lastB != null && lastC != null) value =
                Triple(lastA!!, lastB!!, lastC!!)
        }
    }
}

fun <A, B, C, D> LiveData<A>.combineLatest(
    b: LiveData<B>,
    c: LiveData<C>,
    d: LiveData<D>
): LiveData<NTuple4<A, B, C, D>> {
    return MediatorLiveData<NTuple4<A, B, C, D>>().apply {
        var lastA: A? = null
        var lastB: B? = null
        var lastC: C? = null
        var lastD: D? = null

        addSource(this@combineLatest) {
            if (it == null && value != null) value = null
            lastA = it
            if (lastA != null && lastB != null && lastC != null && lastD != null) value =
                NTuple4(lastA!!, lastB!!, lastC!!, lastD!!)
        }

        addSource(b) {
            if (it == null && value != null) value = null
            lastB = it
            if (lastA != null && lastB != null && lastC != null && lastD != null) value =
                NTuple4(lastA!!, lastB!!, lastC!!, lastD!!)
        }

        addSource(c) {
            if (it == null && value != null) value = null
            lastC = it
            if (lastA != null && lastB != null && lastC != null && lastD != null) value =
                NTuple4(lastA!!, lastB!!, lastC!!, lastD!!)
        }

        addSource(d) {
            if (it == null && value != null) value = null
            lastD = it
            if (lastA != null && lastB != null && lastC != null && lastD != null) value =
                NTuple4(lastA!!, lastB!!, lastC!!, lastD!!)
        }
    }
}

data class NTuple4<T1, T2, T3, T4>(val t1: T1, val t2: T2, val t3: T3, val t4: T4)