package com.adambunim.kaduregel.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.google.android.material.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.button.MaterialButtonToggleGroup

class SingleSelectionMaterialButtonToggleGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.materialButtonToggleGroupStyle
) : MaterialButtonToggleGroup(context, attrs, defStyleAttr) {

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        super.addView(child, params)
        (child as? MaterialButton)?.addOnCheckedChangeListener { button, isChecked ->
            button.isClickable = !isChecked
        }
    }
}
