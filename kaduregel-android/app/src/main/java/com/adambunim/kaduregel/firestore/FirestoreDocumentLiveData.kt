package com.adambunim.kaduregel.firestore

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.ListenerRegistration
import timber.log.Timber

class FirestoreDocumentLiveData(private val ref: DocumentReference) :
    LiveData<DocumentSnapshot>() {

    private var registration: ListenerRegistration? = null
    private val listener =
        EventListener<DocumentSnapshot> { value, error ->
            if (error == null) {
                setValue(value)
            } else {
                Timber.e(error, "Loading snapshot for ref ($ref) failed")
            }
        }

    override fun onActive() {
        registration = ref.addSnapshotListener(listener)
    }

    override fun onInactive() {
        registration?.remove()
        registration = null
    }

}