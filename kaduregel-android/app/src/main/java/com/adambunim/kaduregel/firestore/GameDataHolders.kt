package com.adambunim.kaduregel.firestore

data class GameAndMessages(val game: Game, val messages: List<Message>)
data class MembersWrapper(
    val game: Game,
    val members: List<Member>,
    val showRanks: Boolean,
    val isAdmin: Boolean
)

data class GameAndMembers(val game: Game, val members: List<Member>)
data class GameAndShowRanks(val game: Game, val showRanks: Boolean)
data class MemberAndIsUserAdmin(val member: Member, val isUserAdmin: Boolean)