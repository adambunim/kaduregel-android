package com.adambunim.kaduregel.firestore

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*
import timber.log.Timber

class FirestoreCollectionLiveData(private val query: Query) :
    LiveData<QuerySnapshot>() {

    private var registration: ListenerRegistration? = null
    private val listener =
        EventListener<QuerySnapshot> { value, error ->
            if (error == null) {
                setValue(value)
            } else {
                Timber.e(error, "Loading snapshot for ref ($query) failed")
            }
        }

    override fun onActive() {
        registration = query.addSnapshotListener(listener)
    }

    override fun onInactive() {
        registration?.remove()
        registration = null
    }

}