package com.adambunim.kaduregel.viewmodel

import androidx.lifecycle.*
import com.adambunim.kaduregel.firestore.*

class GamesViewModel(uid: String) : ViewModel() {

    private val liveData = MediatorLiveData<List<GameAndMembers>>().apply {

        val gamesMap = mutableMapOf<Game, List<Member>>()
        var gamesList: List<Game>
        val membersSources: MutableList<LiveData<List<Member>>> = mutableListOf()
        addSource(FirestoreCollectionLiveData(gamesRef().whereArrayContains("memberIds", uid))
            .map { snapshot ->
                snapshot.map { doc ->
                    Game.fromSnapshot(doc)
                }
            }) { games ->
            gamesMap.clear()
            membersSources.forEach { removeSource(it) }
            membersSources.clear()
            gamesList = games
            gamesList.forEach { game ->
                val newSource = FirestoreCollectionLiveData(membersRef(game.id))
                    .map { snapshot ->
                        snapshot.map { doc ->
                            Member.fromSnapshot(doc)
                        }
                    }
                membersSources.add(newSource)
                addSource(newSource) { members ->
                    gamesMap[game] = members
                    if (gamesMap.size == gamesList.size) {
                        value = gamesList.map { game -> GameAndMembers(game, gamesMap[game]!!) }
                    }
                }
            }
        }
    }
        .distinctUntilChanged()

    fun getGamesAndMembers(): LiveData<List<GameAndMembers>> {
        return liveData
    }

}

class GamesViewModelFactory(private val uid: String) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GamesViewModel(uid) as T
    }

}