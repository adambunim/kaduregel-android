package com.adambunim.kaduregel

const val INVITE_SERVER_HOST = "kaduregel-33f50.web.app"
const val INVITE_PARAM_KEY_GAME_ID = "gameid"
const val KEY_GAME_ID = "game_id"
const val KEY_SHOW_RANKS_IN_MEMBERS_LIST = "show_ranks_in_members_list"
const val SHARED_PREFS_NAME = "kaduregel"