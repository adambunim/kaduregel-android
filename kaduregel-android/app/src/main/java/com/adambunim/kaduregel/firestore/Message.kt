package com.adambunim.kaduregel.firestore

import android.annotation.SuppressLint
import com.google.firebase.firestore.DocumentSnapshot
import java.text.SimpleDateFormat
import java.util.*

data class Message(

    val id: String,
    val text: String?,
    val writer: String,
    val writerPretty: String?,
    val picture: String?,
    val created: Long,
    val createdPretty: String?,
    val going: Boolean?,
    val attendIsRelevantFor: String?,
    val timeSent: String?,
    val deleted: Boolean?
) {

    companion object {

        val TEXT = "text"
        val WRITER = "writer"
        val WRITER_PRETTY = "writerPretty"
        val PICTURE = "picture"
        val CREATED = "created"
        val CREATED_PRETTY = "createdPretty"
        val GOING = "going"
        val ATTEND_IS_RELEVANT_FOR = "attendIsRelevantFor"
        val TIME_SENT = "timeSent"
        val DELETED = "deleted"

        @SuppressLint("SimpleDateFormat")
        private val dateFormatter = SimpleDateFormat("HH:mm")
        fun fromSnapshot(doc: DocumentSnapshot): Message {
            val writer = doc.getString(WRITER)!!
            val attendIsRelevantFor = doc.getString(ATTEND_IS_RELEVANT_FOR)
            val created = doc.getLong(CREATED)!! * 1000
            val timeSent = dateFormatter.format(Date(created))
            return Message(
                id = doc.id,
                text = doc.getString(TEXT),
                writer = writer,
                writerPretty = doc.getString(WRITER_PRETTY),
                picture = doc.getString(Member.PICTURE),
                created = created,
                createdPretty = doc.getString(CREATED_PRETTY),
                going = doc.getBoolean(GOING),
                attendIsRelevantFor = attendIsRelevantFor,
                timeSent = timeSent,
                deleted = doc.getBoolean(DELETED)
            )
        }
    }

}