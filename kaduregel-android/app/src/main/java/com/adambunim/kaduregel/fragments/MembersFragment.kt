package com.adambunim.kaduregel.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.adambunim.kaduregel.KEY_GAME_ID
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.adapters.members.MembersAdapter
import com.adambunim.kaduregel.adapters.members.MembersListItem
import com.adambunim.kaduregel.adapters.members.MembersMemberItem
import com.adambunim.kaduregel.adapters.members.MembersTitleItem
import com.adambunim.kaduregel.base.BaseFragment
import com.adambunim.kaduregel.databinding.MembersFragmentBinding
import com.adambunim.kaduregel.firestore.Game
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.Message
import com.adambunim.kaduregel.isGoing
import com.adambunim.kaduregel.viewmodel.MembersViewModel
import com.adambunim.kaduregel.viewmodel.MembersViewModelFactory
import com.adambunim.kaduregel.viewmodel.MessagesViewModel
import com.adambunim.kaduregel.viewmodel.MessagesViewModelFactory

class MembersFragment :
    BaseFragment<MembersFragmentBinding>(
        R.layout.members_fragment,
        MembersFragmentBinding::bind
    ) {

    private lateinit var gameId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = requireArguments()
        gameId = bundle.getString(KEY_GAME_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        val viewModel: MembersViewModel by viewModels {
            MembersViewModelFactory(
                gameId, requireContext().getSharedPreferences(
                    SHARED_PREFS_NAME, Context.MODE_PRIVATE
                )
            )
        }
        viewModel.getGameAndMembers().observe(viewLifecycleOwner
        ) { (game, members, showRanks, isAdmin) ->
            val messagesViewModel: MessagesViewModel by viewModels { MessagesViewModelFactory(gameId) }
            messagesViewModel.getGameAndMessages().observe(
                viewLifecycleOwner
            ) { (_, messages) ->
                fillAdapter(game, members, messages, isAdmin, showRanks)
            }
        }
    }

    private fun setupRecycler() {
        binding.list.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = linearLayoutManager
            adapter = MembersAdapter()
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }

    private fun fillAdapter(
        game: Game,
        members: List<Member>,
        messages: List<Message>,
        isAdmin: Boolean,
        showRanks: Boolean
    ) {
        val noGoing = members.all { member -> !isGoing(member.id, messages) }
        val items: List<MembersListItem> = if (noGoing) {
            members
                .filter { member -> !member.isGuest }
                .map { member ->
                    MembersMemberItem(
                        gameId,
                        member,
                        isAdmin,
                        showRanks
                    )
                }
        } else {
            val colorGoing =
                ResourcesCompat.getColor(resources, R.color.user_going, requireActivity().theme)
            val colorNotGoing =
                ResourcesCompat.getColor(resources, R.color.user_not_going, requireActivity().theme)
            val groups = members.groupBy { isGoing(it.id, messages) }
            val going = (groups[true] ?: emptyList())
                .sortedWith(
                    compareBy { member ->
                        member.team ?: Long.MAX_VALUE
                    }
                )
            val notGoing = (groups[false] ?: emptyList())
                .filter { member -> !member.isGuest }
            listOf(MembersTitleItem(getString(R.string.pattern_going, going.size), colorGoing))
                .plus(going.map { member ->
                    MembersMemberItem(
                        gameId,
                        member,
                        isAdmin,
                        showRanks
                    )
                })
                .plus(
                    MembersTitleItem(
                        getString(R.string.pattern_not_going, notGoing.size),
                        colorNotGoing
                    )
                )
                .plus(notGoing.map { member ->
                    MembersMemberItem(
                        gameId,
                        member,
                        isAdmin,
                        showRanks
                    )
                })
        }
        val adapter = binding.list.adapter as MembersAdapter
        adapter.submitList(items)
    }

}