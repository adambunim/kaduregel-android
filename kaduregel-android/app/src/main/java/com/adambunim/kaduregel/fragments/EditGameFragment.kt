package com.adambunim.kaduregel.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.core.content.edit
import androidx.fragment.app.viewModels
import coil.load
import coil.transform.RoundedCornersTransformation
import com.adambunim.kaduregel.KEY_SHOW_RANKS_IN_MEMBERS_LIST
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.SHARED_PREFS_NAME
import com.adambunim.kaduregel.base.BaseBottomSheetDialogFragment
import com.adambunim.kaduregel.databinding.EditGameFragmentBinding
import com.adambunim.kaduregel.firestore.Game
import com.adambunim.kaduregel.firestore.gameRef
import com.adambunim.kaduregel.firestore.gameStorageRef
import com.adambunim.kaduregel.firestore.uploadPicture
import com.adambunim.kaduregel.ktx.px
import com.adambunim.kaduregel.viewmodel.GameViewModel
import com.adambunim.kaduregel.viewmodel.GameViewModelFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItemsSingleChoice

class EditGameFragment :
    BaseBottomSheetDialogFragment<EditGameFragmentBinding>(
        R.layout.edit_game_fragment,
        EditGameFragmentBinding::bind
    ) {
    private lateinit var game: Game
    private var showRanks = true
    private lateinit var gameId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gameId = requireArguments().getString(KEY_GAME_ID)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gameViewModel: GameViewModel by viewModels {
            GameViewModelFactory(gameId, sharedPreferences)
        }
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        gameViewModel.getGameAndShowRanks().observe(viewLifecycleOwner) { (game, showRanks) ->
            this.game = game
            this.showRanks = showRanks
            binding.nameValue.text = game.name
            binding.maxPlayersValue.text =
                game.maxPlayers?.toString() ?: getString(R.string.no_value)
            binding.maximumRatingValue.text = game.maximumRating?.toString() ?: "3"
            binding.usersViewProfilesValue.text = if (game.usersCanViewProfiles) {
                getString(R.string.users_view_profiles_yes)
            } else {
                getString(R.string.users_view_profiles_no)
            }
            binding.showRanksInListValue.text = if (showRanks) {
                getString(R.string.yes)
            } else {
                getString(R.string.no)
            }
            if (game.picture != null) {
                binding.gamePicture.load(game.picture) {
                    transformations(RoundedCornersTransformation(8.px.toFloat()))
                }
            } else {
                binding.gamePicture.load(R.drawable.ic_football_field)
            }
        }
        binding.editName.setOnClickListener { editName() }
        binding.editMaxPlayers.setOnClickListener { editMaxPlayers() }
        binding.editUsersViewProfiles.setOnClickListener { editUsersViewProfiles() }
        binding.editShowRanksInList.setOnClickListener { editShowRanksInList() }
        binding.editMaximumRating.setOnClickListener { editMaximumRating() }
        binding.editGamePicture.setOnClickListener { editGamePicture() }
    }

    private fun editName() {
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_game_name)
            input(hintRes = R.string.new_game_hint, prefill = game.name) { _, name ->
                val updates = mapOf<String, Any>(
                    Game.NAME to name.toString().trim(),
                )
                gameRef(gameId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editMaxPlayers() {
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_max_players)
            input(
                hintRes = R.string.edit_max_players_hint,
                prefill = game.maxPlayers?.toString(),
                inputType = InputType.TYPE_CLASS_NUMBER
            ) { _, input ->
                val updates = mapOf<String, Any>(
                    Game.MAX_PLAYERS to input.toString().trim().toLong(),
                )
                gameRef(gameId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editUsersViewProfiles() {
        val initialSelection = if (game.usersCanViewProfiles) 0 else 1
        MaterialDialog(requireContext()).show {
            title(R.string.title_edit_users_view_profiles)
            listItemsSingleChoice(
                R.array.users_view_profiles_values,
                initialSelection = initialSelection
            ) { _, index, _ ->
                val enabled = index == 0
                val updates = mapOf<String, Any>(
                    "usersCanViewProfiles" to enabled,
                )
                gameRef(gameId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editShowRanksInList() {
        val initialSelection = if (showRanks) 0 else 1
        MaterialDialog(requireContext()).show {
            title(R.string.title_show_ranks_in_list)
            listItemsSingleChoice(
                R.array.show_ranks_in_list_values,
                initialSelection = initialSelection
            ) { _, index, _ ->
                val showRanks = index == 0
                sharedPreferences.edit {
                    putBoolean(KEY_SHOW_RANKS_IN_MEMBERS_LIST, showRanks)
                }
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editMaximumRating() {
        MaterialDialog(requireContext()).show {
            title(R.string.title_maximum_rating)
            input(
                hintRes = R.string.title_maximum_rating,
                prefill = game.maximumRating?.toString(),
                inputType = InputType.TYPE_CLASS_NUMBER
            ) { _, input ->
                val updates = mapOf<String, Any>(
                    Game.MAXIMUN_RATING to input.toString().trim().toLong(),
                )
                gameRef(gameId).update(updates)
            }
            positiveButton(R.string.save)
            negativeButton(R.string.cancel)
        }
    }

    private fun editGamePicture() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQ_SELECT_PICTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQ_SELECT_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedImageURI: Uri = data?.data!!
                val folderRef = gameStorageRef(gameId)
                uploadPicture(requireContext(), selectedImageURI, folderRef) { photoUri ->
                    val photoUrl = photoUri.toString()
                    val updates = mapOf<String, Any>(
                        Game.PICTURE to photoUrl,
                    )
                    gameRef(gameId).update(updates)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        private const val KEY_GAME_ID = "game_id"
        fun newInstance(gameId: String): EditGameFragment {
            val args = Bundle().apply {
                putString(KEY_GAME_ID, gameId)
            }
            val fragment = EditGameFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val sharedPreferences
        get() = requireContext().getSharedPreferences(
            SHARED_PREFS_NAME, Context.MODE_PRIVATE
        )

}

private const val REQ_SELECT_PICTURE = 1