package com.adambunim.kaduregel.adapters.members

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adambunim.kaduregel.databinding.MemberItemBinding
import com.adambunim.kaduregel.databinding.MembersTitleBinding

enum class MembersListKind {

    TITLE {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                MembersTitleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MembersTitleItem.ViewHolder(binding)
        }
    },
    MEMBER {
        override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
            val binding =
                MemberItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MembersMemberItem.ViewHolder(binding)
        }
    };


    abstract fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}