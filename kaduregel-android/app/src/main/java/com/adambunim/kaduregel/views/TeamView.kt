package com.adambunim.kaduregel.views

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.cardview.widget.CardView
import com.adambunim.kaduregel.databinding.TeamViewBinding
import com.adambunim.kaduregel.drawables.HalfFieldDrawable
import com.adambunim.kaduregel.firestore.Game
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.fragments.MemberFragment
import com.adambunim.kaduregel.ktx.activity
import com.adambunim.kaduregel.ktx.px

class TeamView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    private val binding = TeamViewBinding.inflate(LayoutInflater.from(context), this, true)
    private val memberViews: MutableList<View> = mutableListOf()

    init {

        binding.teamWrapper.background = HalfFieldDrawable(context)
        radius = 8.px.toFloat()
    }

    fun bind(teamColor: Int, members: List<Member>, membersClickable: Boolean, game: Game) {
        memberViews.forEach {
            binding.teamWrapper.removeView(it)
        }
        memberViews.clear()
        binding.teamShirt.imageTintList = ColorStateList.valueOf(teamColor)
        val ids = members.map { member ->
            val view = AvatarAndNameView(context, member)
            val generateViewId = View.generateViewId()
            view.id = generateViewId
            binding.teamWrapper.addView(view)
            memberViews.add(view)
            if (membersClickable) {
                view.setOnClickListener {
                    val memberFragment = MemberFragment.newInstance(game.id, member.id)
                    memberFragment.show(activity.supportFragmentManager, "member_dialog")
                }
            }
            generateViewId
        }
        binding.teamMembersFlow.referencedIds = ids.toIntArray()
    }
}