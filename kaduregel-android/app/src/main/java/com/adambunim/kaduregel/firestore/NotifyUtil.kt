package com.adambunim.kaduregel.firestore

import com.adambunim.kaduregel.PreferencesManager
import com.google.firebase.functions.FirebaseFunctions

fun notifyUsers(text: String, gameId: String) {
    val user = PreferencesManager.currentUser() ?: return
    membersRef(gameId).get().addOnSuccessListener { documents ->
        val members = documents.map { Member.fromSnapshot(it) }
        val iosTokens = members.filter { it.os == "IOS" }.map { it.token }
        val androidTokens = members.filter { it.os == "ANDROID" }.filter { it.id != user.uid }.map { it.token }
        iosTokens.forEach {
            val data = hashMapOf(
                "token" to it,
                "body" to text,
                "title" to "כדורגל"
            )
            FirebaseFunctions.getInstance().getHttpsCallable("notifyIos").call(data)
        }
        androidTokens.forEach {
            val data = hashMapOf(
                "token" to it,
                "body" to text,
                "title" to "כדורגל"
            )
            FirebaseFunctions.getInstance().getHttpsCallable("notifyAndroid").call(data)
        }
    }
}