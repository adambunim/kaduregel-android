package com.adambunim.kaduregel.adapters.messages

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class MessagesAdapter :
    ListAdapter<MessagesListItem, RecyclerView.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MessagesListKind.values()[viewType].createViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position).bindViewHolder(holder)
    }

    override fun getItemViewType(position: Int): Int = getItem(position).kind.ordinal

}

private val diffCallback = object : DiffUtil.ItemCallback<MessagesListItem>() {

    override fun areItemsTheSame(oldItem: MessagesListItem, newItem: MessagesListItem): Boolean =
        oldItem.id == newItem.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: MessagesListItem, newItem: MessagesListItem): Boolean =
        oldItem == newItem

}