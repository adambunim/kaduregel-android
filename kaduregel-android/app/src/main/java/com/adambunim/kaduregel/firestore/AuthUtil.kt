package com.adambunim.kaduregel.firestore

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

fun currentUser(): FirebaseUser? {
    return FirebaseAuth.getInstance().currentUser
}
