package com.adambunim.kaduregel.activities
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.adambunim.kaduregel.R
import androidx.compose.ui.unit.dp

class LoginComposeActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LoginView()
        }
    }
    @Composable
    fun LoginView() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
                Image(
                    painter = painterResource(R.drawable.ic_launcher),
                    contentDescription = "logo",
                    modifier = Modifier
                        .size(200.dp)
                )
            Spacer(modifier = Modifier.size(40.dp))
            Text(text = stringResource(R.string.login_title_normal))
        }

    }

    companion object {
        private const val KEY_IS_INVITE = "is_invite"
        fun newIntent(context: Context, isInvite: Boolean): Intent {
            return Intent(context, LoginComposeActivity::class.java).apply {
                putExtra(KEY_IS_INVITE, isInvite)
            }
        }
    }

    @Preview
    @Composable
    fun PreviewLoginCard() {
        LoginView()
    }

}