package com.adambunim.kaduregel.viewmodel

import androidx.lifecycle.*
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.firestore.FirestoreDocumentLiveData
import com.adambunim.kaduregel.firestore.Member
import com.adambunim.kaduregel.firestore.memberRef

class IsAdminViewModel(gameId: String) : ViewModel() {

    private val liveData = FirestoreDocumentLiveData(memberRef(gameId, PreferencesManager.currentUser()!!.uid))
        .map { snapshot ->
            val user = Member.fromSnapshot(snapshot)
            user.admin
        }
        .distinctUntilChanged()

    fun getIsAdmin(): LiveData<Boolean> {
        return liveData
    }

}

class IsAdminViewModelFactory(private val gameId: String) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return IsAdminViewModel(gameId) as T
    }

}