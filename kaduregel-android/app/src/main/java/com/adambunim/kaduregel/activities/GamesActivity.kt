package com.adambunim.kaduregel.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.adambunim.kaduregel.PreferencesManager
import com.adambunim.kaduregel.R
import com.adambunim.kaduregel.adapters.games.GamesAdapter
import com.adambunim.kaduregel.base.BaseActivity
import com.adambunim.kaduregel.databinding.ActivityGamesBinding
import com.adambunim.kaduregel.firestore.*
import com.adambunim.kaduregel.ktx.enforceSingleScrollDirection
import com.adambunim.kaduregel.viewmodel.GamesViewModel
import com.adambunim.kaduregel.viewmodel.GamesViewModelFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import timber.log.Timber
import java.security.SecureRandom

class GamesActivity : BaseActivity<ActivityGamesBinding>(ActivityGamesBinding::inflate) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.main_menu)
        binding.toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.create -> {
                    openCreateGameDialog()
                    true
                }
                R.id.logout -> {
                    FirebaseAuth.getInstance().signOut()
                    true
                }
                else -> false
            }
        }
        binding.createGameButton.setOnClickListener { openCreateGameDialog() }
        setupRecycler()
        val user = PreferencesManager.currentUser()
        if (user != null) {
            val viewModel: GamesViewModel by viewModels { GamesViewModelFactory(user.uid) }
            viewModel.getGamesAndMembers().observe(this,
                { games ->
                    val adapter = binding.gamesList.adapter as GamesAdapter
                    adapter.submitList(games)
                    binding.gamesEmpty.isVisible = games.isEmpty()
                    binding.gamesList.isVisible = games.isNotEmpty()
                })
        }
    }

    private fun setupRecycler() {
        binding.gamesList.apply {
            layoutManager = LinearLayoutManager(this@GamesActivity)
            adapter = GamesAdapter()
            enforceSingleScrollDirection()
        }
    }

    private fun openCreateGameDialog() {
        MaterialDialog(this).show {
            title(R.string.new_game)
            input(hintRes = R.string.new_game_hint) { _, name ->
                createGame(name.toString())
            }
            positiveButton(R.string.new_game_positive)
            negativeButton(R.string.cancel)
        }
    }

    private fun createGame(name: String) {
        val user = PreferencesManager.currentUser() ?: return
        val game = hashMapOf(
            Game.NAME to name,
            Game.MEMBER_IDS to listOf(user.uid),
            Game.LINK to randomLink()
        )

        gamesRef().add(game)
            .addOnSuccessListener { documentReference ->
                val member = hashMapOf(
                    Member.NAME to user.displayName,
                    Member.ADMIN to true
                )
                memberRef(documentReference.id, user.uid).set(member)
                //GameSettingsVC.setLink(gameRef.documentID)
            }
            .addOnFailureListener { e ->
                Timber.w(e, "Error adding document")
            }
    }

    override fun onStart() {
        super.onStart()
    }

    private fun randomLink(): String {
        val len = 5
        val ab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        val rnd = SecureRandom()
        val sb = StringBuilder(len)
        for (i in 0 until len) {
            sb.append(ab[rnd.nextInt(ab.length)])
        }
        return sb.toString()
    }

}