package com.adambunim.kaduregel.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.adambunim.kaduregel.*
import com.adambunim.kaduregel.firestore.*
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.Date

class MainEmptyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val user = currentUser()
        if (user != null) {
            PreferencesManager.setUid(user.uid)
            user.displayName?.let {
                PreferencesManager.setDisplayName(it)
            }
        }

        val isInvite = intent.data?.host == INVITE_SERVER_HOST
        if (isInvite) {
            val gameId = intent.data!!.getQueryParameter(INVITE_PARAM_KEY_GAME_ID)
            if (gameId.isNullOrEmpty()) {
                showToast(getString(R.string.invite_error_bad_link))
                handleNormalRoute()
            } else {
                if (user == null) {
                    val intent = LoginActivity.newIntent(this, true)
                    //val intent = LoginComposeActivity.newIntent(this, true)
                    startActivityForResult(intent, REQ_AUTH_FOR_INVITE)
                } else {
                    processInvite(gameId)
                }
            }
        } else {
            handleNormalRoute()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQ_AUTH_FOR_INVITE) {
            if (resultCode == RESULT_OK) {
                val gameId = intent.data!!.getQueryParameter(INVITE_PARAM_KEY_GAME_ID)!!
                processInvite(gameId)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun processInvite(gameId: String) {
        val currentUser = PreferencesManager.currentUser()!!
        gameRef(gameId).get().addOnSuccessListener { snapshot ->
            val game = Game.fromSnapshot(snapshot)
            when {
                game.memberIds.contains(currentUser.uid) -> {
                    showToast(getString(R.string.invite_error_already_member))
                    handleNormalRoute()
                }
                else -> {
                    Firebase.firestore.runBatch { batch ->
                        batch.update(
                            gameRef(gameId),
                            Game.MEMBER_IDS,
                            FieldValue.arrayUnion(currentUser.uid)
                        )
                        val memberMap = hashMapOf(
                            Member.NAME to currentUser.displayName,
                            Member.OS to "ANDROID",
                            Member.LAST_SEEN to now(),
                            Member.LAST_SEEN_DATE to Date(),
                            Member.VERSION_NAME to BuildConfig.VERSION_NAME
                        )
                        batch.set(memberRef(gameId, currentUser.uid), memberMap)
                    }
                        .addOnSuccessListener {
                            showToast(getString(R.string.invite_success, game.name))
                            handleNormalRoute()
                        }
                }
            }

        }
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    private fun handleNormalRoute() {
        if (PreferencesManager.currentUser() == null) {
            //startActivity(LoginComposeActivity.newIntent(this, false))
            startActivity(LoginActivity.newIntent(this, false))
        } else {
            startActivity(Intent(this, GamesActivity::class.java))
        }
        finish()
    }
}

private const val REQ_AUTH_FOR_INVITE = 2